package com.jlarguelloymalondono.prnapp.Main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.jlarguelloymalondono.prnapp.Informacion.Informacion_General
import com.jlarguelloymalondono.prnapp.Pantalla_Principal.Pantalla_principal
import com.jlarguelloymalondono.prnapp.Registro.RegistroDatos
import com.jlarguelloymalondono.prnapp.Registro.RegistroViewModel
import com.jlarguelloymalondono.prnapp.Scaffold.SScafold
import com.jlarguelloymalondono.prnapp.SobreNosotros.Sobre_nosotros
import com.jlarguelloymalondono.prnapp.ui.theme.ERCAPPTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val myViewModel = RegistroViewModel(
            application = application,
        )
        super.onCreate(savedInstanceState)
        setContent {
            ERCAPPTheme {
                // A surface container using the 'background' color from the themes
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navigationController = rememberNavController()

                    NavHost(navController = navigationController, startDestination = "s8"){
                        composable("s1"){ Pantalla_principal(navigationController) }
                        composable("s2"){ Informacion_General(navigationController, myViewModel) }
                        composable("s3"){ RegistroDatos(navigationController, myViewModel) }
                        composable("s4"){ Sobre_nosotros(navigationController, myViewModel) }
                        composable("s8"){ SScafold(navigationController, myViewModel) }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    ERCAPPTheme {
        Greeting("Android")
    }
}