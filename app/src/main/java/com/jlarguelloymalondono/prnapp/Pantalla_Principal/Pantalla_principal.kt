package com.jlarguelloymalondono.prnapp.Pantalla_Principal

import android.annotation.SuppressLint
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.jlarguelloymalondono.prnapp.R


@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun Pantalla_principal(navController: NavHostController) {

    var option by remember {
        mutableStateOf(0)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(
                color = Color(android.graphics.Color.parseColor("#eef5fd")),
                shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp)
            )
            .verticalScroll(rememberScrollState()),
    ) {
        Spacer(modifier = Modifier.height(180.dp))

        val fontFamily = FontFamily(Font(R.font.simplysansbold))
        Text(
            text = "    Categorías",
            fontFamily = fontFamily,
            fontWeight = FontWeight.Bold
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            CenteredCard(R.drawable.twotone_info_242, "Información") {
                option = 0
                navController.navigate("s2")
            }
            CenteredCard(R.drawable.twotone_medical_information_242, "Mi registro") {
                option = 3
                navController.navigate("s3")
            }
            CenteredCard(R.drawable.twotone_people_242, "Sobre \nNosotros") {
                option = 5
                navController.navigate("s4")
            }
        }

        Spacer(modifier = Modifier.height(30.dp))

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
        ) {
            CenteredCard2(R.drawable.ucat){
            }
            Spacer(modifier = Modifier.height(10.dp))
        }
        Spacer(modifier = Modifier.height(80.dp))
    }
}



//Scaffold

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar() {
    TopAppBar(
        modifier = Modifier
            .height(150.dp)
            .background(
                color = Color(android.graphics.Color.parseColor("#5ec468")),
                shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp)
            ),
        title = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxSize()
                    .clip(RoundedCornerShape(bottomEnd = 30.dp, bottomStart = 30.dp)),
                contentAlignment = Alignment.Center
            ) {
                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                Text(
                    text = "PERFILADOR DE \n RIESGO \n NUTRICIONAL",
                    fontFamily = fontFamily,
                    color = androidx.compose.ui.graphics.Color.White,
                    textAlign = TextAlign.Center,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    lineHeight = 24.sp
                )
            }
        },
        colors = TopAppBarDefaults.smallTopAppBarColors(
            containerColor = androidx.compose.ui.graphics.Color.Transparent,
            titleContentColor = androidx.compose.ui.graphics.Color.LightGray,
            navigationIconContentColor = androidx.compose.ui.graphics.Color.White
        )
    )
}





// "Botones"/cards


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CenteredCard(@DrawableRes iconResId: Int, text: String, onClick: () -> Unit) {
    Card(
        modifier = Modifier
            .width(120.dp)
            .height(130.dp)
            .padding(8.dp)
            .background(Color.White, shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp, topStart = 16.dp, topEnd = 16.dp)),
            onClick = onClick
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White, shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp, topStart = 16.dp, topEnd = 16.dp)),
        ) {

            Spacer(modifier = Modifier.height(6.dp))

            Image(
                painter = painterResource(iconResId),
                contentDescription = null,
                modifier = Modifier.size(48.dp)
            )
            Spacer(modifier = Modifier.height(10.dp))

            val fontFamily = FontFamily(Font(R.font.simplysansbold))
            Text(
                text = text,
                fontFamily = fontFamily,
                textAlign = TextAlign.Center,

            )

        }
    }
}




@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CenteredCard2(@DrawableRes iconResId: Int, onClick: () -> Unit) {
    Card(
        modifier = Modifier
            .width(3000.dp)
            .height(130.dp)
            .padding(8.dp)
            .background(Color.White, shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp, topStart = 16.dp, topEnd = 16.dp)),
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = Color(android.graphics.Color.parseColor("#ffffff")),
                    shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp, topStart = 16.dp, topEnd = 16.dp)
                )
        ) {
            Spacer(modifier = Modifier.height(6.dp))
            Image(
                painter = painterResource(iconResId),
                contentDescription = null,
                modifier = Modifier.size(250.dp)
            )
            Spacer(modifier = Modifier.height(10.dp))
        }
    }
}


