package com.jlarguelloymalondono.prnapp.Registro

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "registro")
data class Registro(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val sexo: String,
    val peso: Float,
    val estatura: Float,
    val edad_dias: Float
)

