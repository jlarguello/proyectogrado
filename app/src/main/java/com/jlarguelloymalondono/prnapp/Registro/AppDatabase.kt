package com.jlarguelloymalondono.prnapp.Registro

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Registro::class, PredicResponseEntity::class], version =1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun RegistroDAO(): RegistroDAO
    abstract fun predicResponseDao(): PredicResponseDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "app_database1"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}
