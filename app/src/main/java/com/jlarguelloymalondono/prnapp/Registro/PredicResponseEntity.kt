package com.jlarguelloymalondono.prnapp.Registro

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "predic_response")
data class PredicResponseEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val infanteId: Int,
    val talla_predicha: String,
    val peso_predicho: String,
    val peso_talla_predicho: String,
    val fecha_prediccion: String
)
