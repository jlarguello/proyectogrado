package com.jlarguelloymalondono.prnapp.Registro

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface RegistroDAO {

    @Insert()
    suspend fun insertRegistro(registro: Registro)

    @Query("SELECT * FROM registro")
    suspend fun getRegistro():List<Registro>

    @Delete
    suspend fun deleteRegistro(friend: Registro)

    @Update
    suspend fun updateRegistro(registro: Registro)

}

