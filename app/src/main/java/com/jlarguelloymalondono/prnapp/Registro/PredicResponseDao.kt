package com.jlarguelloymalondono.prnapp.Registro

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PredicResponseDao {
    @Insert
    suspend fun insertPredicResponse(response: PredicResponseEntity): Long

    @Query("SELECT * FROM predic_response")
    suspend fun getAllPredicResponses(): List<PredicResponseEntity>
}