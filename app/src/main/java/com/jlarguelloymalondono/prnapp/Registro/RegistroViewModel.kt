package com.jlarguelloymalondono.prnapp.Registro

import android.app.Application
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.jlarguelloymalondono.prnapp.Modelo.PredicRequest
import com.jlarguelloymalondono.prnapp.Modelo.PredicResponse
import com.jlarguelloymalondono.prnapp.Modelo.Servicio
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class RegistroViewModel(application: Application) : AndroidViewModel(application) {

    val servicio = Servicio.create()

    private val dao: RegistroDAO = AppDatabase.getDatabase(application).RegistroDAO()
    private val predicResponseDao = AppDatabase.getDatabase(application).predicResponseDao()

    val registrosState: MutableStateFlow<List<Registro>> = MutableStateFlow(emptyList())

    val predicResponseState: MutableStateFlow<PredicResponse?> = MutableStateFlow(null)
    val prediccionesState: MutableStateFlow<List<PredicResponseEntity>> = MutableStateFlow(emptyList())

    var sexo by mutableStateOf("")
        private set

    fun setSexoValue(newValue: String) {
        sexo = newValue
    }

    var peso by mutableStateOf("")
        private set

    fun setPesoValue(newValue: String) {
        peso = newValue
    }

    var estatura by mutableStateOf("")
        private set

    fun setEstaturaValue(newValue: String) {
        estatura = newValue
    }

    var edad_dias by mutableStateOf("")
        private set

    fun setEdadDiasValue(newValue: String) {
        edad_dias = newValue
    }

    init {
        // Inicializar los registros al inicio
        viewModelScope.launch {
            cargarRegistros()
            cargarPredicciones()
        }
    }


    fun addRegistro() {
        viewModelScope.launch {
            val pesoValue = peso.toFloatOrNull()
            val estaturaValue = estatura.toFloatOrNull()
            val edadDiasValue = edad_dias.toFloatOrNull()

            if (pesoValue != null && estaturaValue != null && edadDiasValue != null) {
                dao.insertRegistro(Registro(sexo = sexo, peso = pesoValue, estatura = estaturaValue, edad_dias = edadDiasValue))
                cargarRegistros()
            } else {

            }
        }
    }

    fun getRegistro() {
        viewModelScope.launch {
            cargarRegistros()
        }
    }


    private suspend fun cargarRegistros() {
        val nuevosRegistros = dao.getRegistro()
        registrosState.value = nuevosRegistros
    }

    private suspend fun cargarPredicciones() {
        val nuevasPredicciones = predicResponseDao.getAllPredicResponses()
        prediccionesState.value = nuevasPredicciones
    }


    fun deleteRegistro(registro: Registro) {
        viewModelScope.launch {
            try {
                dao.deleteRegistro(registro)
                cargarRegistros()
            } catch (ex: Exception) {

            }
        }
    }

    fun postClasificacion(registro: Registro) {
        val request = PredicRequest(registro.sexo, registro.peso, registro.estatura, registro.edad_dias)
        viewModelScope.launch {
            try {
                val response = servicio.postpredic(request)
                predicResponseState.value = response

                predicResponseDao.insertPredicResponse(
                    PredicResponseEntity(
                        infanteId = registro.id,
                        talla_predicha = response.talla_predicha,
                        peso_predicho = response.peso_predicho,
                        peso_talla_predicho = response.peso_talla_predicho,
                        fecha_prediccion = response.fecha_prediccion
                    )
                )
                cargarPredicciones()
            } catch (e: Exception) {
                Log.e("RegistroViewModel", "Error al obtener predicción: ${e.message}")
                e.printStackTrace()
            }
        }
    }


    var registro by mutableStateOf(listOf<Registro>())
        private set


    fun onNameChange(newName: String) {
        sexo = newName
    }

    fun onPesoChange(newPeso: String) {
        peso = newPeso
    }

    fun onEstaturaChange(newEstatura: String) {
        estatura = newEstatura
    }

    fun onEdadDiasChange(newEdadDias: String) {
        edad_dias = newEdadDias
    }


}
