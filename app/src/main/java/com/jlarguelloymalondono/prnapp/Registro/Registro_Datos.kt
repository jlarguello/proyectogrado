package com.jlarguelloymalondono.prnapp.Registro

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.jlarguelloymalondono.prnapp.Scaffold.BottomBar
import com.jlarguelloymalondono.prnapp.Informacion.Informacion_General
import com.jlarguelloymalondono.prnapp.Pantalla_Principal.Pantalla_principal
import com.jlarguelloymalondono.prnapp.Pantalla_Principal.TopBar
import com.jlarguelloymalondono.prnapp.R
import com.jlarguelloymalondono.prnapp.SobreNosotros.Sobre_nosotros

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegistroDatos(navigationController: NavHostController, myViewModel: RegistroViewModel) {

    var option by remember {
        mutableStateOf(0)
    }

    Scaffold(
        topBar = { TopBar() },
        bottomBar = { BottomBar(option = option) { newOption -> option = newOption } },
        content = {
            when (option) {
                1 -> {
                    Pantalla_principal(navigationController)
                }
                2 -> Informacion_General(navigationController, myViewModel)
                3 -> RegistroDatos(navigationController, myViewModel)
                4 -> Sobre_nosotros(navigationController, myViewModel)
            }

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = 150.dp, bottom = 56.dp)

            ) {
                if (option == 0) {
                    Home(myViewModel)
                }
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Home(myViewModel: RegistroViewModel) {

    val registrosState by myViewModel.registrosState.collectAsState()

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        content = {
            // Índice de los elementos en LazyColumn
            item {
                MostrarFormulario(myViewModel)
            }

            itemsIndexed(registrosState) { index, item ->
                MostrarTarjeta(index, item, myViewModel)
            }

            item {
                Spacer(modifier = Modifier.height(8.dp))
            }
        },
        contentPadding = PaddingValues(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MostrarFormulario(myViewModel: RegistroViewModel) {

    val sexOptions = listOf("M", "F")
    var expanded by remember { mutableStateOf(false) }

    Spacer(modifier = Modifier.height(8.dp))
    val fontFamily = FontFamily(Font(R.font.simplysansbold))

    Box(
        modifier = Modifier
            .padding(8.dp)
            .width(200.dp)
            .offset(x = (-8).dp)
    ) {
        OutlinedTextField(
            value = myViewModel.sexo,
            onValueChange = { },
            label = { Text("Sexo", fontFamily = fontFamily) },
            readOnly = true,
            trailingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_add_circle_24),
                    contentDescription = null,
                    modifier = Modifier.clickable { expanded = true }
                )
            },
            modifier = Modifier.width(200.dp) // Define el mismo ancho para el TextField
        )
    }
    DropdownMenu(
        expanded = expanded,
        onDismissRequest = { expanded = false },
        modifier = Modifier.fillMaxWidth(),
        offset = DpOffset(0.dp, 100.dp)
    ) {
        sexOptions.forEach { sex ->
            DropdownMenuItem(
                text = { Text(text = sex, fontFamily = fontFamily) },
                onClick = {
                    myViewModel.onNameChange(sex)
                    expanded = false
                }
            )
        }
    }

    TextField(
        value = myViewModel.peso,
        onValueChange = { myViewModel.onPesoChange(it) },
        label = { Text("Peso", fontFamily = fontFamily) },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number
        )
    )

    TextField(
        value = myViewModel.estatura,
        onValueChange = { myViewModel.onEstaturaChange(it) },
        label = { Text("Estatura", fontFamily = fontFamily) },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number
        )
    )

    TextField(
        value = myViewModel.edad_dias,
        onValueChange = { myViewModel.onEdadDiasChange(it) },
        label = { Text("Edad en dias", fontFamily = fontFamily) },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number
        )
    )

    Spacer(modifier = Modifier.height(5.dp))

    Button(onClick = {
        myViewModel.addRegistro()
        myViewModel.setSexoValue("")
        myViewModel.setPesoValue("")
        myViewModel.setEstaturaValue("")
        myViewModel.setEdadDiasValue("")
    }) {
        Text(text = "Guardar Registro", fontFamily = fontFamily)
    }

}

@Composable
fun MostrarTarjeta(index: Int, item: Registro, myViewModel: RegistroViewModel) {

    val predicResponse by myViewModel.predicResponseState.collectAsState()
    val predicciones by myViewModel.prediccionesState.collectAsState()

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
    ) {
        Column {
            Text(text = "#$index", fontFamily = FontFamily.SansSerif)

            Spacer(modifier = Modifier.height(5.dp))

            Text(text = "Sexo: ${item.sexo}",fontFamily = FontFamily.SansSerif)
            Text(text = "Peso: ${item.peso}",fontFamily = FontFamily.SansSerif)
            Text(text = "Estatura: ${item.estatura}",fontFamily = FontFamily.SansSerif)
            Text(text = "Edad en dias: ${item.edad_dias}",fontFamily = FontFamily.SansSerif)

            predicciones.find { it.infanteId == item.id }?.let {
                Spacer(modifier = Modifier.height(16.dp))

                Text(text = "Talla Predicha: ", fontFamily = FontFamily.SansSerif)
                Text(text = it.talla_predicha, color = getColorBasedOnResult(it.talla_predicha), fontFamily = FontFamily.SansSerif)

                Text(text = "Peso Predicho: ", fontFamily = FontFamily.SansSerif)
                Text(text = it.peso_predicho, color = getColorBasedOnResult(it.peso_predicho), fontFamily = FontFamily.SansSerif)

                Text(text = "Peso/Talla Predicho: ", fontFamily = FontFamily.SansSerif)
                Text(text = it.peso_talla_predicho, color = getColorBasedOnResult(it.peso_talla_predicho), fontFamily = FontFamily.SansSerif)
                Text(text = "Fecha de Clasificación: ${it.fecha_prediccion}")
            }

            Spacer(modifier = Modifier.height(10.dp))

            Button(onClick = { myViewModel.postClasificacion(item) }) {
                Text("Obtener Clasificación")
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                horizontalArrangement = Arrangement.Start
            ) {
                Spacer(modifier = Modifier.weight(1f))
                Icon(
                    painter = painterResource(id = R.drawable.twotone_delete_24),
                    contentDescription = null,
                    modifier = Modifier.size(24.dp)
                        .clickable {
                            myViewModel.deleteRegistro(item)
                        }
                )
            }
        }
    }
    Spacer(modifier = Modifier.height(8.dp))
}

@Composable
fun getColorBasedOnResult(result: String): Color {

    val LightGreen = Color(0xFF81C784)
    val DarkRed = Color(0xFFD32F2F)
    val LightBlue = Color(0xFF64B5F6)
    val DefaultTextColor = Color(0xFF000000)


    return when (result) {
        "Sobrepeso" -> DarkRed
        "Saludable" -> LightGreen
        "Desnutrición" -> LightBlue
        else -> DefaultTextColor
    }
}
