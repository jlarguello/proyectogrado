package com.jlarguelloymalondono.prnapp.Informacion

data class ContenidoInfo(val titulo:String, val info: String)
data class ContenidoInfo2(val titulo:String, val info: String)
data class ContenidoInfo3(val titulo:String, val info: String)
data class ContenidoInfo4(val titulo:String, val info: String)
data class ContenidoInfo5(val titulo:String, val info: String)
data class ContenidoInfo6(val titulo:String, val info: String)
data class ContenidoInfo7(val titulo:String, val info: String)
data class ContenidoInfo8(val titulo:String, val info: String)
data class ContenidoInfo9(val titulo:String, val info: String)



val Contenido = mutableListOf<ContenidoInfo>(
    ContenidoInfo("Indicador Antropometrico",
        "Es un índice estadístico que surge de la combinación de dos variables o parámetros que se utiliza para medir o evaluar cuantitativamente el crecimiento y el estado nutricional, toma como base medidas corporales y se obtiene mediante la comparación, contra valores de referencia para la edad y sexo o contra mediciones realizadas en el mismo sujeto en diferentes períodos.")
)

val Contenido2 = mutableListOf<ContenidoInfo2>(
    ContenidoInfo2("Puntuación Z (Z Score)",
        "Es la diferencia entre el valor individual y el valor medio de la población de referencia, para la misma edad o talla, dividido entre la desviación estándar de la población de referencia, es decir, identifica cuán lejos de la mediana (de la población de referencia) se encuentra el valor individual obtenido.")
)

val Contenido3 = mutableListOf<ContenidoInfo3>(
    ContenidoInfo3("Peso para la edad - P/E",
    "Indicador antropométrico que relaciona el peso con la edad sin considerar la talla. ")
)

val Contenido4 = mutableListOf<ContenidoInfo4>(
    ContenidoInfo4("Peso para la longitud/talla - P/T",
    "Un indicador de crecimiento que relaciona el peso con longitud o con la talla. Da cuenta del estado nutricional actual del individuo.")
)

val Contenido5 = mutableListOf<ContenidoInfo5>(
    ContenidoInfo5("Talla para la Edad - T/E",
    "un indicador de crecimiento que relaciona la talla o longitud con la edad. Da cuenta del estado nutricional histórico o acumulativo.")
)

val Contenido6 = mutableListOf<ContenidoInfo6>(
    ContenidoInfo6("IMC para la Edad - IMC/E",
    "Indice de Masa Corporal es un indicador que correlaciona de acuerdo con la edad, el peso corporal total en relación a la talla. Se obtiene al dividir el peso expresado en kilogramos entre la talla expresada en metros al cuadrado")
)

val Contenido7 = mutableListOf<ContenidoInfo7>(
    ContenidoInfo7("Desnutrición",
    "Por debajo de la línea de puntuación -2 desviaciones estándar de puntuación Z en los indicadores peso para la edad, peso para la longitud/talla, longitud/talla para la edad o IMC para la edad.")
)

val Contenido8 = mutableListOf<ContenidoInfo8>(
    ContenidoInfo8("Sobrepeso infantil",
    "Peso para la longitud/talla o IMC para la edad entre las líneas de puntuación Z >+2 y ≤+3 desviaciones estándar en menores de cinco años y >+1 y ≤+2 desviaciones estándar del indicador IMC/E en el grupo de edad de 5 a 17 años.")
)

val Contenido9 = mutableListOf<ContenidoInfo9>(
    ContenidoInfo9("Graficas",
    "Las gráficas muestran canales de crecimiento, los que están destacados con líneas curvas. La mediana de cada indicador de acuerdo con la referencia OMS 2006-2007 aparece representada por una línea más gruesa de color verde y se identifica por el número cero (0). Las líneas de color amarillo situadas sobre la mediana y por debajo de la mediana respectivamente corresponden a +1 y -1 DE. Las líneas punteadas de color rojo corresponden a +2 y -2 DE, y " +
            "las líneas continuas también de color rojo corresponden a +3 y -3 DE. La zona entre +1 y –1 DE corresponde al rango donde se espera ubicar la mayor cantidad de individuos, sin embargo, factores genéticos o valores de peso y talla de nacimiento fuera del rango habitual" +
            "pueden determinar diferentes canales de crecimiento, lo que debe ser analizado a través de un diagnóstico nutricional y de salud más completo que incluya antecedentes de la gestación, peso al nacer, tendencia y velocidad de crecimiento, situación de salud, lactancia materna y también de factores socioeconómicos."

))



