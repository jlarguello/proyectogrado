package com.jlarguelloymalondono.prnapp.Informacion

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.jlarguelloymalondono.prnapp.Scaffold.BottomBar
import com.jlarguelloymalondono.prnapp.Pantalla_Principal.Pantalla_principal
import com.jlarguelloymalondono.prnapp.Pantalla_Principal.TopBar
import com.jlarguelloymalondono.prnapp.Registro.RegistroDatos
import com.jlarguelloymalondono.prnapp.SobreNosotros.Sobre_nosotros
import com.jlarguelloymalondono.prnapp.Registro.RegistroViewModel


@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun Informacion_General(navigationController: NavHostController, myViewModel: RegistroViewModel) {

    var option by remember {
        mutableStateOf(0)
    }

    Scaffold(
        topBar = { TopBar() },
        bottomBar = { BottomBar(option = option) { newOption -> option = newOption} },
        content = {
            when (option) {
                1 -> {
                    Pantalla_principal(navigationController)
                }
                2 -> Informacion_General(navigationController, myViewModel)
                3 -> RegistroDatos(navigationController, myViewModel)
                4 -> Sobre_nosotros(navigationController, myViewModel)
            }

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = 150.dp, bottom = 56.dp)
            ) {
                if (option == 0) {
                    CardInfo()
                }
            }
        }
    )
}



@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar2() {
    TopAppBar(
        modifier = Modifier
            .height(150.dp)
            .background(
                color = Color(android.graphics.Color.parseColor("#6863e3")),
                shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp)
            ),
        title = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxSize()
                    .clip(RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp)),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "ERCAPP",
                    color = androidx.compose.ui.graphics.Color.White,
                    textAlign = TextAlign.Center,
                    fontSize = 60.sp,
                    fontWeight = FontWeight.Bold
                )
            }
        },
        colors = TopAppBarDefaults.smallTopAppBarColors(
            containerColor = androidx.compose.ui.graphics.Color.Transparent,
            titleContentColor = androidx.compose.ui.graphics.Color.LightGray,
            navigationIconContentColor = androidx.compose.ui.graphics.Color.White
        )
    )
}

