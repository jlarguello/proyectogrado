package com.jlarguelloymalondono.prnapp.Informacion

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.jlarguelloymalondono.prnapp.R


@Preview
@Composable
fun CardInfo() {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(
                color = Color(android.graphics.Color.parseColor("#eef5fd")),
                shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp)
            ),
    ) {
        item {

            Spacer(modifier = Modifier.height(19.dp))

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            )

            {
                Contenido.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Image(
                    painter = painterResource(id = R.drawable.tabla1),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(16f / 9f) // Establece la relación de aspecto deseada
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.FillBounds
                )

                Contenido2.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Spacer(modifier = Modifier.height(8.dp))

                Image(
                    painter = painterResource(id = R.drawable.tabla2),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.Crop
                )

                Contenido3.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Image(
                    painter = painterResource(id = R.drawable.tabla4),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.Crop
                )

                Image(
                    painter = painterResource(id = R.drawable.tabla6),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.Crop
                )

                Contenido4.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Image(
                    painter = painterResource(id = R.drawable.tabla5),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.Crop
                )

                Image(
                    painter = painterResource(id = R.drawable.tabla7),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.Crop
                )

                Contenido5.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Image(
                    painter = painterResource(id = R.drawable.tabla3),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.Crop
                )

                Image(
                    painter = painterResource(id = R.drawable.tabla8),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = MaterialTheme.shapes.medium),
                    contentScale = ContentScale.Crop
                )

                Contenido6.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Contenido7.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Contenido8.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                Spacer(modifier = Modifier.height(8.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

                Contenido9.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.White)
                                .padding(16.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.White)
                            ) {
                                val fontFamily = FontFamily(Font(R.font.simplysansbold))
                                Text(
                                    text = titulo,
                                    fontFamily = fontFamily,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Bold,
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )

                                Spacer(modifier = Modifier.height(16.dp))

                                Text(
                                    text = info,
                                    fontFamily = FontFamily.SansSerif,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(8.dp)
                                        .align(Alignment.CenterHorizontally),
                                    style = TextStyle(
                                        fontSize = 18.sp,
                                        textAlign = TextAlign.Justify
                                    )
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }

            }
        }
    }
}