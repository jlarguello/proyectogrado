package com.jlarguelloymalondono.prnapp.SobreNosotros

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.jlarguelloymalondono.prnapp.R


@Preview
@Composable

fun Cardpmex() {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(
                color = Color(android.graphics.Color.parseColor("#eef5fd")),
                shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp)
            ),
    ) {
        item {

            Spacer(modifier = Modifier.height(19.dp))

            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(20.dp)
            ) {
                Box(
                    modifier = Modifier.fillMaxSize()
                        .background(color = Color(android.graphics.Color.parseColor("#f8ccc6"))),
                    contentAlignment = Alignment.Center

                ) {
                    val fontFamily = FontFamily(Font(R.font.simplysansbold))
                    Text(text = "Desarrolladores", fontSize = 25.sp,fontFamily = fontFamily,
                        fontWeight = FontWeight.Bold)
                }
            }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(0.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = Modifier.height(19.dp))

                Colombia.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp)
                        ) {
                            Row(
                                modifier = Modifier.fillMaxWidth()
                                    .background(color = Color(android.graphics.Color.parseColor("#f8ccc6"))),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                // Imagen a la izquierda
                                Image(
                                    painterResource(id = imgmex),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .size(100.dp)
                                        .clip(CircleShape)
                                        .background(MaterialTheme.colorScheme.primary)
                                        .align(Alignment.CenterVertically),
                                    contentScale = ContentScale.Crop
                                )

                                // Nombre y código a la derecha
                                Column(
                                    modifier = Modifier
                                        .padding(start = 16.dp)
                                        .align(Alignment.CenterVertically)
                                        .background(color = Color(android.graphics.Color.parseColor("#f8ccc6"))),
                                ) {
                                    Text(text = nombre, textAlign = TextAlign.Center, fontSize = 25.sp, fontFamily = FontFamily.SansSerif)
                                    Text(text = codigo, textAlign = TextAlign.Center, fontSize = 20.sp)
                                }
                            }
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                    }
                }
            }


            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(20.dp)
            ) {

            }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(0.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                informacion.forEach {
                    with(it) {
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp)
                        ) {
                            Row(
                                modifier = Modifier.fillMaxWidth()
                                    .background(color = Color(android.graphics.Color.parseColor("#f8ccc6"))),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                // Imagen a la izquierda

                                // Nombre y código a la derecha
                                Column(
                                    modifier = Modifier
                                        .padding(start = 16.dp)
                                        .align(Alignment.CenterVertically)
                                        .background(color = Color(android.graphics.Color.parseColor("#f8ccc6"))),
                                ) {
                                    Text(text = nombre, textAlign = TextAlign.Center, fontSize = 25.sp, fontFamily = FontFamily.SansSerif)
                                }
                            }
                        }
                        Spacer(modifier = Modifier.height(19.dp))
                    }
                }
                Spacer(modifier = Modifier.height(19.dp))
            }




        }
    }
}


