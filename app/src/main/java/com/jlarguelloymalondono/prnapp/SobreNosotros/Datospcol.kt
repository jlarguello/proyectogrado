package com.jlarguelloymalondono.prnapp.SobreNosotros

import com.jlarguelloymalondono.prnapp.R


data class Datospcol( val nombre :String, val codigo: String, val imgmex: Int )

val Colombia = mutableListOf<Datospcol>(
    Datospcol("Jorge Luis Arguello Avila", "67000583", R.drawable.captura_de_pantalla_de_2023_11_29_11_27_06),
    Datospcol("Miguel Angel Londoño Alvarez", "67000585", R.drawable.c4),
)