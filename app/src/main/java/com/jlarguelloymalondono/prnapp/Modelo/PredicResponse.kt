package com.jlarguelloymalondono.prnapp.Modelo

data class PredicResponse(
    val infante: Infante,
    val talla_predicha: String,
    val peso_predicho: String,
    val peso_talla_predicho: String,
    val fecha_prediccion: String
)