package com.jlarguelloymalondono.prnapp.Modelo

data class PredicRequest(
    val sexo:String,
    val peso: Float,
    val estatura: Float,
    val edad_dias: Float
)

