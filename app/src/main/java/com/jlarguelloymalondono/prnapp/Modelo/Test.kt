package com.jlarguelloymalondono.prnapp.Modelo

import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    val servicio = Servicio.create()
    val requestBody = PredicRequest(
        sexo = "M",
        peso = 21.0f,
        estatura = 119.0f,
        edad_dias = 1825.0f
    )
    try {
        val response = servicio.postpredic(requestBody)
        println(response)
    } catch (e: Exception) {
        println("Error: ${e.message}")
    }
}