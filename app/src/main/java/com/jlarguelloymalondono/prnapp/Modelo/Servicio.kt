package com.jlarguelloymalondono.prnapp.Modelo

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST


interface Servicio {

    @POST("predecir/")
    suspend fun postpredic(@Body requestBody: PredicRequest): PredicResponse

    companion object {
        fun create(): Servicio {
            return Retrofit.Builder()
                .baseUrl("http://jlarguello83.pythonanywhere.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Servicio::class.java)
        }
    }
}