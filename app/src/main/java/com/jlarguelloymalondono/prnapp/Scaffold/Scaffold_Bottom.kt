package com.jlarguelloymalondono.prnapp.Scaffold

import android.annotation.SuppressLint
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.navigation.NavHostController
import com.jlarguelloymalondono.prnapp.Informacion.Informacion_General
import com.jlarguelloymalondono.prnapp.Pantalla_Principal.Pantalla_principal
import com.jlarguelloymalondono.prnapp.Pantalla_Principal.TopBar
import com.jlarguelloymalondono.prnapp.Registro.RegistroDatos
import com.jlarguelloymalondono.prnapp.Registro.RegistroViewModel
import com.jlarguelloymalondono.prnapp.SobreNosotros.Sobre_nosotros
import com.jlarguelloymalondono.prnapp.R


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BottomBar(option : Int, onOptionSelected:(Int)-> Unit) {

    BottomAppBar(
        containerColor = Color(android.graphics.Color.parseColor("#5ec468")),
        contentColor = androidx.compose.ui.graphics.Color.White,
    ) {
        NavigationBarItem(
            selected = option == 1,
            onClick = { onOptionSelected(1) },
            icon = {
                Icon(
                    imageVector = Icons.Default.Home,
                    contentDescription = "Llevar al inicio",
                    tint = androidx.compose.ui.graphics.Color.White
                )
            },
            label = { val fontFamily = FontFamily(Font(R.font.simplysansbold)); Text(text = "Inicio", fontFamily = fontFamily, fontWeight = FontWeight.Bold) }
        )

        NavigationBarItem(
            selected = option == 2,
            onClick = { onOptionSelected(2) },
            icon = {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_library_books_24),
                    contentDescription = "Informacion general",
                    tint = androidx.compose.ui.graphics.Color.White
                )
            },
            label = { val fontFamily = FontFamily(Font(R.font.simplysansbold));Text(text = "Información", fontFamily = fontFamily, fontWeight = FontWeight.Bold)  }
        )

        NavigationBarItem(
            selected = option == 3,
            onClick = { onOptionSelected(3)},
            icon = {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_medical_information_24),
                    contentDescription = "Registro de Sistole y Diastole",
                    tint = androidx.compose.ui.graphics.Color.White
                )
            },
            label = { val fontFamily = FontFamily(Font(R.font.simplysansbold));Text(text = "Mi registro", fontFamily = fontFamily, fontWeight = FontWeight.Bold) }
        )

        NavigationBarItem(
            selected = option == 4,
            onClick = { onOptionSelected(4)},
            icon = {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_person_24),
                    contentDescription = "Perfiles de los estudiantes de Mexico y Colombia",
                    tint = androidx.compose.ui.graphics.Color.White
                )
            },
            label = { val fontFamily = FontFamily(Font(R.font.simplysansbold));Text(text = "Perfiles", fontFamily = fontFamily, fontWeight = FontWeight.Bold)  }
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
fun SScafold(navController: NavHostController, myViewModel: RegistroViewModel) {

    var option by remember {
        mutableStateOf(1)
    }

    Scaffold(
        topBar = { TopBar() },
        bottomBar = { BottomBar(option = option) { newOption -> option = newOption } },
        content = {
            when (option) {
                1 -> Pantalla_principal(navController)
                2 -> Informacion_General(navController, myViewModel)
                3 -> RegistroDatos(navController, myViewModel)
                4 -> Sobre_nosotros(navController, myViewModel)
            }
        }
    )
}
